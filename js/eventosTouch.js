var xi, yi, xf, yf;
var cuadrito = document.getElementById("areaDibujo");
var papel = cuadrito.getContext("2d");
var colorcito;
var lapizBajado;

function bajarLapiz(e) {
	xi = e.targetTouches[0].pageX;
  yi = e.targetTouches[0].pageY;
	colorcito = "green";
	console.log("bajó lapiz");
	lapizBajado = true;
}

function trazar(e) {
	if(lapizBajado){
	xf = e.targetTouches[0].pageX;
  yf = e.targetTouches[0].pageY;
	dibujarLinea(colorcito, xi, yi, xf, yf, papel);
	xi = xi + (xf - xi);
	yi = yi + (yf - yi);
  console.log("trazando");
	}
}

function dibujarLinea(color, xInicial, yInicial, xFinal, yFinal, lienzo) {
  lienzo.beginPath();
  lienzo.strokeStyle = color;
  lienzo.lineWidth = 1;
  lienzo.moveTo(xInicial, yInicial);
  lienzo.lineTo(xFinal, yFinal);
  lienzo.stroke();
  lienzo.closePath();
}

function levantarLapiz() {
	lapizBajado = false;
	console.log("soltó lapiz");
}

function asustar(elem){
  elem.src = "img/scare.jpg";
}

function sonreir(elem){
  elem.src = "img/smile.png";
}
