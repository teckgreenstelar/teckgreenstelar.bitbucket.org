var xi, yi, xf, yf;
var cuadrito = document.getElementById("areaDibujo");
var papel = cuadrito.getContext("2d");
var colorcito;
var lapizBajado;

function bajarLapiz(e) {
	xi = e.clientX;
  yi = e.clientY;
	colorcito = "green";
	console.log("bajó lapiz");
	lapizBajado = true;
}

function trazar(e) {
	if(lapizBajado){
	xf = e.clientX;
  yf = e.clientY;
	dibujarLinea(colorcito, xi, yi, xf, yf, papel);
	xi = xi + (xf - xi);
	yi = yi + (yf - yi);
	}
}

function dibujarLinea(color, xInicial, yInicial, xFinal, yFinal, lienzo) {
  lienzo.beginPath();
  lienzo.strokeStyle = color;
  lienzo.lineWidth = 1;
  lienzo.moveTo(xInicial, yInicial);
  lienzo.lineTo(xFinal, yFinal);
  lienzo.stroke();
  lienzo.closePath();
}

function levantarLapiz() {
	lapizBajado = false;
	console.log("soltó lapiz");
}
